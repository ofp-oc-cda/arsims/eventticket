import { CustomError } from "../interfaces";

export class DatabaseValidationError extends CustomError {
  reason = "Error connecting to database ... ";
  statusCode = 500;
  constructor() {
    super("Error connecting to database ... ");
    // We onlywrite this because we are extending a build in class
    Object.setPrototypeOf(this, DatabaseValidationError.prototype);
  }

  serializeErrors() {
    return [
      {
        message: this.reason,
      },
    ];
  }
}
