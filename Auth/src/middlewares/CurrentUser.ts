import { Request, Response, NextFunction } from "express";
import jwt from "jsonwebtoken";
import { UserPayload } from "../interfaces";

declare module "express-serve-static-core" {
  export interface Request {
    currentUser?: UserPayload;
  }
}

export const curentUserMiddleware = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  if (!req.session?.jwt) {
    return next();
  }

  try {
    const payload = jwt.verify(req.session.jwt, process.env.JWT_KEY!) as UserPayload;
    req.currentUser = payload;
    console.log("hej")
  } catch (err) {}

  next();
};
