import express, { Request, Response } from "express";
import "express-async-errors";
import jwt from "jsonwebtoken";
import { ExpressValidator } from "../constants";
import { BadRequestError } from "../errors";
import { ValidateRequest } from "../middlewares";
import { PasswordHash } from "../services";
import { User } from "../models/UserModel";

const router = express.Router();

router.post(
  "/api/users/signin",
  ExpressValidator,
  ValidateRequest,
  async (req: Request, res: Response) => {
    const { email, password, username } = req.body;

    const existingUser = await User.findOne({ email });
    if (!existingUser) {
      throw new BadRequestError(`Invalid credentials`);
    }

    const paswordsMatch = await PasswordHash.compare(
      existingUser.password,
      password
    );

    if(!paswordsMatch) {
        throw new BadRequestError(`Invalid credentials`);
    }

    // Generate JWT
    const userJWT = jwt.sign(
      {
        id: existingUser.id,
        email: existingUser.email,
        username: existingUser.username,
      },
      process.env.JWT_KEY!
    );

    // Store it on session object
    req.session = {
      jwt: userJWT,
    };

    res.status(200).send(existingUser);
  }
);

export { router as signinRouter };
