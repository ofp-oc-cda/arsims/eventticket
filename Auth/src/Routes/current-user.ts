import { Request, Response, Router } from "express";
import { curentUserMiddleware } from "../middlewares";

const router = Router();

router.get(
  "/api/users/currentuser",
  curentUserMiddleware,
  (req: Request<{'currentUser': string}>, res: Response) => {
    res.send({ currentUser: JSON.stringify(req.currentUser) });
  }
);

export { router as currentUserRouter };
