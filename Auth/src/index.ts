import express, { NextFunction, Request, Response } from "express";
import "express-async-errors";
import { json } from "body-parser";
import cors from "cors";
import mongoose from "mongoose";
import cookieSession from "cookie-session";

import {
  currentUserRouter,
  signinRouter,
  signoutRouter,
  signupRouter,
} from "./Routes";

//middleware
import { errorHandlers } from "./middlewares";
import { DatabaseValidationError, NotFoundError } from "./errors";

const app = express();
app.set('trust proxy', true)
app.use(json());
app.use(cors());
app.use(cookieSession({
  signed: false,
  secure: true,

}))

app.use(currentUserRouter);
app.use(signinRouter);
app.use(signoutRouter);
app.use(signupRouter);

app.all("*", async () => {
  throw new NotFoundError();
});

//error handler
app.use(errorHandlers);

const start = async () => {
  const PORT = process.env.PORT || 3000;
  if(!process.env.JWT_KEY){
    throw new Error('JWT_KEY must be defined')
  }
  try {
    await mongoose.connect("mongodb://auth-mongo-srv:27017/auth");
    console.log("Connecting to MongoDB ... ")
  } catch (error) {
    throw new DatabaseValidationError().reason;
    console.error(error)
  }

  app.listen(PORT, () => {
    console.log(`[Auth Server] - Listening on port ${PORT}`);
  });
};

start();
